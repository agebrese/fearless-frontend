function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col-4">
        <div class="card shadow p-3 mb-5 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-secondary">${location}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer">
                ${starts}-${ends}
              </div>
            </div>
        </div>
    </div>
    `;
}

function errorCard() {
    return `
    <div class="alert alert-danger" role="alert">
        Incorrect URL entered for API
    </div>
    `;
}

function urlErrorCard() {
    return `
    <div class="alert alert-danger" role="alert">
        Incorrect URL Entered
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            const html = errorCard();
            const div = document.querySelector(".container")
            div.innerHTML = html;
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts).toLocaleDateString("en-us");
                    const endDate = new Date(details.conference.ends).toLocaleDateString("en-us");
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;

                }
            }
        }
    } catch (e) {
        console.error(e)
        // Figure out what to do if an error is raised
        const html = urlErrorCard();
        const div = document.querySelector(".container")
        div.innerHTML = html;
    }

});
